# Localized Chat
**Text Chat** talk within a range, disabling global chat and adding a few handy commands to assist chat.;

This pairs really well with [Simple Voice Chat](https://www.curseforge.com/minecraft/mc-mods/simple-voice-chat) by [henkelmax](https://www.curseforge.com/members/henkelmax/).

# Original Mod: WhisperingShout 
https://minecraft.curseforge.com/projects/whisperingshout MIT Licensed

# Side
Server, not required on client.

#Commands
## Shout
Says something to all players on the server, uses hunger!

/shout <Message ... ... ...>

/s <Message ... ... ...>

/sh <Message ... ... ...>

## Talk -- no command needed
Says something to all players within 100 blocks of you. **configurable**

